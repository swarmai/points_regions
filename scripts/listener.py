#!/usr/bin/env python

import rospy
from points_regions.msg import *
from pymavlink import mavutil

#from points_regions.treedict import *

def connect_to_drone(address = 'tcp:127.0.0.1:5762'):
    drone = mavutil.mavlink_connection(address)
    # Wait a heartbeat before sending commands
    drone.wait_heartbeat()
    return drone

def arm(drone):
    drone.mav.command_long_send(
    1, # autopilot system id
    1, # autopilot component id
    400, # command id, ARM/DISARM
    0, # confirmation
    1, # arm!
    0,0,0,0,0,0 # unused parameters for this command
    )

def disarm(drone):
    drone.mav.command_long_send(
    1, # autopilot system id
    1, # autopilot component id
    400, # command id, ARM/DISARM
    0, # confirmation
    0, # disarm!
    0,0,0,0,0,0) # unused parameters for this command

def takeoff(alt, drone):
    drone.mav.command_long_send(
    1, # autopilot system id
    0, # autopilot component id
    22, # command id, MAV_CMD_NAV_TAKEOFF
    0, # confirmation
    0, # unused 
    0, # 
    0,0,0,0,alt # alt in metres
    )

def set_mode(mode, drone):

    # Check if mode is available
    if mode not in drone.mode_mapping():
        #print('Unknown mode : {}'.format(mode))
        #print('Try:', list(drone.mode_mapping().keys()))
        exit(1)

    # Get mode ID
    mode_id = drone.mode_mapping()[mode]
    # Set new mode
    # drone.mav.command_long_send(
    #    drone.target_system, drone.target_component,
    #    mavutil.mavlink.MAV_CMD_DO_SET_MODE, 0,
    #    0, mode_id, 0, 0, 0, 0, 0) or:
    # drone.set_mode(mode_id) or:
    drone.mav.set_mode_send(
        drone.target_system,
        mavutil.mavlink.MAV_MODE_FLAG_CUSTOM_MODE_ENABLED,
        mode_id)

    # Check ACK
    ack = False
    while not ack:
        # Wait for ACK command
        ack_msg = drone.recv_match(type='COMMAND_ACK', blocking=True)
        ack_msg = ack_msg.to_dict()

        # Check if command in the same in `set_mode`
        if ack_msg['command'] != mavutil.mavlink.MAVLINK_MSG_ID_SET_MODE:
            continue

        # Print the ACK result !
        #print(mavutil.mavlink.enums['MAV_RESULT'][ack_msg['result']].description)
        break

def liftoff():
    drone = connect_to_drone()
    set_mode("GUIDED", drone)
    arm(drone)
    takeoff(5, drone)
    set_mode("AUTO", drone)

class Point:
    def __init__(self,lat,lon,alt,index,group,region,region_index):
        self.region_index=region_index
        self.region=region
        self.group=group
        self.lat=lat
        self.long=lon
        self.alt=alt
        self.index=index
    def __str__(self):
        return ("Region Index = "+str(self.region_index)+
        ", Region = "+str(self.region)+
        ", Group = "+str(self.group)+
        ", Lat = "+str(self.lat)+
        ", Long = "+str(self.long)+
        ", Alt = "+str(self.alt)+
        ", Index = "+str(self.index))

SendableMission=[]
WorkingMission=[]
CompleteMission=[]
last_index = -1

def callback(pointstream):
    global WorkingMission
    global SendableMission
    global CompleteMission
    
    WorkingMission.append(Point(pointstream.lat,pointstream.long,pointstream.alt,pointstream.index,  
        pointstream.group, pointstream.region, pointstream.region_index))

    takeoff = False
    if takeoff == False and pointstream.index < 0:
        liftoff()
        takeoff == True

    if pointstream.index==pointstream.end_index:
        CompleteMission=WorkingMission
        WorkingMission=[]

def listener():

    # In ROS, nodes are uniquely named. If two nodes with the same
    # name are launched, the previous one is kicked off. The
    # anonymous=True flag means that rospy will choose a unique
    # name for our 'listener' node so that multiple listeners can
    # run simultaneously.
    rospy.init_node('listener', anonymous=True)

    rospy.Subscriber('chatter', pointstream, callback)
    while True:
        raw_input("Press enter to view most recent mission")
        for point in CompleteMission:
            print(point)
    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()

if __name__ == '__main__':
    
    listener()
