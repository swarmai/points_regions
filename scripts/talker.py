#!/usr/bin/env python

import rospy
from points_regions.msg import *
from random import *

pointindex=[]

def talker():
    pub = rospy.Publisher('chatter', pointstream, queue_size=10)
    rospy.init_node('talker', anonymous=True)
    rate = rospy.Rate(2) # 10hz
    global pointindex
    pointindex.append([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
    pointindex.append([-1, 1, 2, 3, 4, 5, 6, 7, 9, 10])
    pointindex.append([0, 1, 2, 3, 4, 5, 6, 7, 9, 10, 11])
    i=0
    j=0
    #for this to work properly you would need to 
    #check the length of the list you put in and 
    #make sure it doesnt change while you send it
    
    while not rospy.is_shutdown():
        point = pointstream(pointindex[j][i],0,0,0,20.14568252,49.58542121,30,pointindex[j][-1])
        i=i+1

        if i>len(pointindex[j])-1:
            i=0
            j=j+1
            if j>len(pointindex)-1:
                j=0
        rospy.loginfo(point)
        pub.publish(point)
        rate.sleep()

if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass
